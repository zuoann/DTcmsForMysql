/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `dtcms` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dtcms`;

CREATE TABLE IF NOT EXISTS `dt_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) NOT NULL,
  `salt` varchar(20) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(20) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `avatar` varchar(255) DEFAULT '',
  `nick_name` varchar(100) DEFAULT '',
  `sex` varchar(20) DEFAULT '',
  `birthday` datetime DEFAULT NULL,
  `telphone` varchar(50) DEFAULT '',
  `area` varchar(255) DEFAULT '',
  `address` varchar(255) DEFAULT '',
  `qq` varchar(20) DEFAULT '',
  `msn` varchar(100) DEFAULT '',
  `amount` decimal(9,2) DEFAULT NULL,
  `point` int(10) DEFAULT NULL,
  `exp` int(10) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `reg_time` datetime DEFAULT current_timestamp(),
  `reg_ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USERS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `dt_users`;
/*!40000 ALTER TABLE `dt_users` DISABLE KEYS */;
INSERT INTO `dt_users` (`id`, `site_id`, `group_id`, `user_name`, `salt`, `password`, `mobile`, `email`, `avatar`, `nick_name`, `sex`, `birthday`, `telphone`, `area`, `address`, `qq`, `msn`, `amount`, `point`, `exp`, `status`, `reg_time`, `reg_ip`) VALUES
	(1, 1, 1, 'test', '4R04B6', '6F0936315BA3646A', '13695245546', 'test@qq.com', '', '测试用户', '保密', NULL, '', '', '', '', '', 0.00, 10, 10, 0, '2017-04-19 02:32:10', '127.0.0.1');
/*!40000 ALTER TABLE `dt_users` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_addr_book` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `accept_name` varchar(100) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT '',
  `telphone` varchar(30) DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `post_code` varchar(20) DEFAULT '',
  `is_default` tinyint(3) unsigned DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_ADDR_BOOK` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_addr_book`;
/*!40000 ALTER TABLE `dt_user_addr_book` DISABLE KEYS */;
INSERT INTO `dt_user_addr_book` (`id`, `user_id`, `user_name`, `accept_name`, `area`, `address`, `mobile`, `telphone`, `email`, `post_code`, `is_default`, `add_time`) VALUES
	(1, 1, 'test', '测试人', '广东省,深圳市,宝安区', '西乡街道西乡街道33号', '13800138000', '', 'test@qq.com', '', 0, '2017-05-04 01:56:08');
/*!40000 ALTER TABLE `dt_user_addr_book` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_amount_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `value` decimal(9,2) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_AMOUNT_LOG` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_amount_log`;
/*!40000 ALTER TABLE `dt_user_amount_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_amount_log` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_attach_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `attach_id` int(10) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_dt_user_attach_log` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_attach_log`;
/*!40000 ALTER TABLE `dt_user_attach_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_attach_log` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_code` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `str_code` varchar(255) DEFAULT NULL,
  `count` int(10) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `user_ip` varchar(20) DEFAULT NULL,
  `eff_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `add_time` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_CODE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_code`;
/*!40000 ALTER TABLE `dt_user_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_code` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '',
  `grade` int(10) DEFAULT NULL,
  `upgrade_exp` int(10) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `point` int(10) DEFAULT NULL,
  `discount` int(10) DEFAULT NULL,
  `is_default` tinyint(3) unsigned DEFAULT NULL,
  `is_upgrade` tinyint(3) unsigned DEFAULT NULL,
  `is_lock` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_GROUPS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_groups`;
/*!40000 ALTER TABLE `dt_user_groups` DISABLE KEYS */;
INSERT INTO `dt_user_groups` (`id`, `title`, `grade`, `upgrade_exp`, `amount`, `point`, `discount`, `is_default`, `is_upgrade`, `is_lock`) VALUES
	(1, '注册会员', 1, 0, 0.00, 0, 100, 1, 0, 0),
	(2, '高级会员', 2, 1000, 0.00, 0, 99, 0, 1, 0),
	(3, '中级会员', 3, 2000, 1.00, 10, 98, 0, 1, 0),
	(4, '钻石会员', 4, 3000, 2.00, 20, 95, 0, 1, 0);
/*!40000 ALTER TABLE `dt_user_groups` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_group_price` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) DEFAULT NULL,
  `article_id` int(10) DEFAULT NULL,
  `goods_id` int(10) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `price` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_GROUP_PRICE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_group_price`;
/*!40000 ALTER TABLE `dt_user_group_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_group_price` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_login_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT '',
  `remark` varchar(255) DEFAULT '',
  `login_time` datetime DEFAULT current_timestamp(),
  `login_ip` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_LOGIN_LOG` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_login_log`;
/*!40000 ALTER TABLE `dt_user_login_log` DISABLE KEYS */;
INSERT INTO `dt_user_login_log` (`id`, `user_id`, `user_name`, `remark`, `login_time`, `login_ip`) VALUES
	(1, 1, 'test', '会员登录', '2017-05-04 01:47:46', '127.0.0.1');
/*!40000 ALTER TABLE `dt_user_login_log` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_message` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) unsigned DEFAULT NULL,
  `post_user_name` varchar(100) DEFAULT NULL,
  `accept_user_name` varchar(100) DEFAULT NULL,
  `is_read` tinyint(3) unsigned DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `post_time` datetime NOT NULL DEFAULT current_timestamp(),
  `read_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_MESSAGE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_message`;
/*!40000 ALTER TABLE `dt_user_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_message` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_oauth` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `oauth_name` varchar(50) NOT NULL DEFAULT '0',
  `oauth_access_token` varchar(500) DEFAULT NULL,
  `oauth_openid` varchar(255) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_OAUTH` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_oauth`;
/*!40000 ALTER TABLE `dt_user_oauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_oauth` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_point_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `value` int(10) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_POINT_LOG` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_point_log`;
/*!40000 ALTER TABLE `dt_user_point_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_point_log` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_user_recharge` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `recharge_no` varchar(100) DEFAULT NULL,
  `payment_id` int(10) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  `complete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_USER_RECHARGE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_user_recharge`;
/*!40000 ALTER TABLE `dt_user_recharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_user_recharge` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_weixin_access_token` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) DEFAULT NULL,
  `access_token` varchar(1000) DEFAULT NULL,
  `expires_in` int(10) DEFAULT NULL,
  `count` int(10) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_WEIXIN_ACCESS_TOKEN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_weixin_access_token`;
/*!40000 ALTER TABLE `dt_weixin_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_weixin_access_token` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_weixin_account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `originalid` varchar(50) DEFAULT NULL,
  `wxcode` varchar(50) DEFAULT NULL,
  `token` text DEFAULT NULL,
  `appid` varchar(100) DEFAULT NULL,
  `appsecret` varchar(150) DEFAULT NULL,
  `is_push` tinyint(3) unsigned DEFAULT NULL,
  `sort_id` int(10) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_WEIXIN_ACCOUNT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_weixin_account`;
/*!40000 ALTER TABLE `dt_weixin_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_weixin_account` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_weixin_request_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) DEFAULT NULL,
  `rule_id` int(10) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `link_url` text DEFAULT NULL,
  `img_url` text DEFAULT NULL,
  `media_id` varchar(255) DEFAULT NULL,
  `media_url` text DEFAULT NULL,
  `meida_hd_url` text DEFAULT NULL,
  `sort_id` int(10) DEFAULT NULL,
  `add_time` timestamp(6) NULL DEFAULT current_timestamp(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_WEIXIN_REQUEST_CONTENT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_weixin_request_content`;
/*!40000 ALTER TABLE `dt_weixin_request_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_weixin_request_content` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_weixin_request_rule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `keywords` text DEFAULT NULL,
  `request_type` int(10) DEFAULT NULL,
  `response_type` int(10) DEFAULT NULL,
  `is_like_query` tinyint(3) unsigned DEFAULT NULL,
  `is_default` tinyint(3) unsigned DEFAULT NULL,
  `sort_id` int(10) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_WEIXIN_REQUEST_RULE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_weixin_request_rule`;
/*!40000 ALTER TABLE `dt_weixin_request_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_weixin_request_rule` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `dt_weixin_response_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) DEFAULT NULL,
  `openid` varchar(100) DEFAULT NULL,
  `request_type` varchar(50) DEFAULT NULL,
  `request_content` varchar(2000) DEFAULT NULL,
  `response_type` varchar(50) DEFAULT NULL,
  `reponse_content` varchar(2000) DEFAULT NULL,
  `create_time` varchar(50) DEFAULT NULL,
  `xml_content` varchar(2000) DEFAULT NULL,
  `add_time` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `PK_DT_WEIXIN_RESPONSE_CONTENT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE FROM `dt_weixin_response_content`;
/*!40000 ALTER TABLE `dt_weixin_response_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_weixin_response_content` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
